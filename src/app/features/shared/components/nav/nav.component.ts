import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromApp from '../../../../store/app.reducer';
import * as AuthActions from '../../../auth/store/auth.actions';
import {Subscription} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  @Input() isAuthenticated = false;

  constructor(
    private store: Store<fromApp.AppState>
  ) {}

  onLogout() {
    this.store.dispatch(AuthActions.logout());
  }
}
