export interface ErrorResponse {
  error: {
    error: string;
    message: string;
    statusCode: number;
  }
}
