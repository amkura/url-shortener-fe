export interface PaginationData {
  items: any[],
  count: number,
  total: number,
}
