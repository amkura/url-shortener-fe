import {Component, ViewEncapsulation} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import * as fromApp from '../../../../store/app.reducer';
import * as UrlActions from '../../store/actions/url.actions';

@Component({
  selector: 'app-url-shortener-form',
  templateUrl: './url-shortener-form.component.html',
  styleUrls: ['./url-shortener-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UrlShortenerFormComponent {

  constructor(
    private store: Store<fromApp.AppState>
  ) {
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(UrlActions.shortenUrlStart(form.value));
  }
}
