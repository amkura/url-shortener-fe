import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import * as fromApp from '../../../../store/app.reducer';
import * as UrlActions from '../../store/actions/url.actions';
import {map} from "rxjs/operators";
import {ShortenedUrl} from "../../shortened-url.model";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-url-list',
  templateUrl: './url-list.component.html',
  styleUrls: ['./url-list.component.scss']
})
export class UrlListComponent implements OnInit, OnDestroy {

  public shortenedUrls: ShortenedUrl[] = [];
  public page = 1;
  public itemsPerPage = 5;
  public totalItems = 0;

  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<fromApp.AppState>
  ) {
  }

  ngOnInit() {
    this.dispatchGetPaginatedShortenedUrls();
    this.watchShortenedUrlsChanges();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  watchShortenedUrlsChanges() {
    this.subscription = this.store.select('url')
      .pipe(
        map((urlState) => urlState.shortenedUrls)
      )
      .subscribe(shortenedUrls => {
        this.shortenedUrls = shortenedUrls.items;
        this.totalItems = shortenedUrls.total;
      });
  }

  onDeleteUrlItem(urlId: string) {
    this.store.dispatch(UrlActions.deleteUrlStart({id: urlId}));
  }

  onPageChange(newPage: number) {
    this.page = newPage;
    this.dispatchGetPaginatedShortenedUrls();
  }

  dispatchGetPaginatedShortenedUrls() {
    this.store.dispatch(UrlActions.getShortenedUrlsStart({
      offset: (this.page - 1) * this.itemsPerPage,
      limit: this.itemsPerPage
    }));
  }
}
