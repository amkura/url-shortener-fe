import {Component, Input, EventEmitter, Output} from '@angular/core';
import {Clipboard} from "@angular/cdk/clipboard";
import {ShortenedUrl} from "../../../shortened-url.model";

@Component({
  selector: 'app-url-item',
  templateUrl: './url-item.component.html',
  styleUrls: ['./url-item.component.scss']
})
export class UrlItemComponent {

  @Input() shortenedUrl!: ShortenedUrl;

  @Output() delete = new EventEmitter<string>();

  constructor(private clipboard: Clipboard) {}

  public onCopyShortUrlToClipboard(): void {
    this.clipboard.copy(this.shortenedUrl.shortUrl);
  }

  public onDelete(): void {
    this.delete.emit(this.shortenedUrl._id);
  }
}
