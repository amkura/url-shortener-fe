import {Component} from '@angular/core';
import {Store} from "@ngrx/store";
import * as fromApp from '../../../store/app.reducer';
import * as UrlActions from '../store/actions/url.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.container.html',
  styleUrls: ['./home.container.scss']
})
export class HomeContainerComponent {

}
