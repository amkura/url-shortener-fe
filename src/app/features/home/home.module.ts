import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../auth/auth.guard";
import {UrlShortenerFormComponent} from "./components/url-shortener-form/url-shortener-form.component";
import {FormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import { HomeContainerComponent } from './components/home.container';
import {MatCardModule} from "@angular/material/card";

const COMPONENTS = [
  UrlShortenerFormComponent,
  HomeContainerComponent,
];

const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: HomeContainerComponent}
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    RouterModule.forChild(routes),
  ],
  providers: [],
})
export class HomeModule { }
