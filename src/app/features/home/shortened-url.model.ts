export interface ShortenedUrl {
  _id: string;
  shortUrl: string;
  longUrl: string;
  title: string;
}
