import {  createAction, props } from "@ngrx/store";
import {ShortenedUrl} from "../../shortened-url.model";
import { PaginationData } from '../../../shared/models/pagination-data.model';

export const shortenUrlStart = createAction(
  '[Url] Shorten Start',
  props<{
    title: string,
    url: string
  }>()
);

export const shortenUrlSuccess = createAction(
  '[Url] Shorten Success',
  props<{ shortenedUrl: ShortenedUrl }>()
);

export const shortenUrlFail = createAction(
  '[Url] Shorten Fail',
  props<{ message: string; }>()
);

export const deleteUrlStart = createAction(
  '[Url] Delete Start',
  props<{ id: string }>()
);

export const deleteUrlSuccess = createAction(
  '[Url] Delete Success',
  props<{ id: string }>()
);

export const deleteUrlFail = createAction(
  '[Url] Delete Fail',
  props<{ message: string; }>()
);

export const getShortenedUrlsStart = createAction(
  '[Url] Get Shortened Start',
  props<{
    offset: number;
    limit: number;
  }>()
);

export const getShortenedUrlsSuccess = createAction(
  '[Url] Get Shortened Success',
  props<{ shortenedUrls: PaginationData }>()
);

export const getShortenedUrlsFail = createAction(
  '[Url] Get Shortened Fail',
  props<{ message: string; }>()
);

