import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as UrlActions from '../actions/url.actions';
import {HttpClient} from "@angular/common/http";
import {catchError, map, switchMap} from 'rxjs/operators';
import { of } from "rxjs";
import {Injectable} from "@angular/core";
import {ShortenedUrl} from "../../shortened-url.model";
import {PaginationData} from "../../../shared/models/pagination-data.model";
import { environment } from "../../../../../environments/environment";
import {ErrorResponse} from "../../../shared/models/error-response.model";


@Injectable()
export class UrlEffects {

  routeURL = environment.API;

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {}

  shortenUrlStart$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(UrlActions.shortenUrlStart),
        switchMap((action) => {
          const { title, url } = action;
          return this.http
            .post<ShortenedUrl>(`${this.routeURL}/v1/urls`, { title, url })
            .pipe(
              map(resData => UrlActions.shortenUrlSuccess({shortenedUrl: resData})),
              catchError((error: ErrorResponse) => of(UrlActions.shortenUrlFail({message: error.error.message})))
            )
        })
      )
  )

  deleteUrlStart$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(UrlActions.deleteUrlStart),
        switchMap((action) => {
          return this.http
            .delete(`${this.routeURL}/v1/urls/${action.id}`)
            .pipe(
              map(resData => UrlActions.deleteUrlSuccess({ id: action.id })),
              catchError((error: ErrorResponse) => of(UrlActions.deleteUrlFail({message: error.error.message})))
            )
        })
      )
  )

  getShortenedUrlsStart$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(UrlActions.getShortenedUrlsStart),
        switchMap((action) => {
          return this.http
            .get<PaginationData>(`${this.routeURL}/v1/urls`, { params: { offset: action.offset, limit: action.limit }})
            .pipe(
              map(paginationData => UrlActions.getShortenedUrlsSuccess({shortenedUrls: paginationData})),
              catchError((error: ErrorResponse) =>  of(UrlActions.getShortenedUrlsFail({message: error.error.message})))
            )
        })
      )
  )
}
