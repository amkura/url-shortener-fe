import { createReducer, on, Action } from "@ngrx/store"
import * as UrlActions from "../actions/url.actions";
import {ShortenedUrl} from "../../shortened-url.model";
import {PaginationData} from "../../../shared/models/pagination-data.model";

export interface State {
  error: string | null;
  loading: boolean;
  shortenedUrls: PaginationData
}

const initialState: State = {
  error: null,
  loading: false,
  shortenedUrls: {
    items: [],
    count: 0,
    total: 0
  }
}

const _urlReducer = createReducer(
  initialState,
  on(
    UrlActions.shortenUrlStart,
    (state) => ({
      ...state,
      error: null,
      loading: true
    })
  ),
  on(
    UrlActions.shortenUrlSuccess,
    (state, payload) => ({
      ...state,
      loading: false,
      shortenedUrls: {
        items: [
          payload.shortenedUrl,
          ...state.shortenedUrls.items
        ],
        count: state.shortenedUrls.count + 1,
        total: state.shortenedUrls.total + 1
      }
    })
  ),
  on(
    UrlActions.shortenUrlFail,
    (state, payload) => ({
      ...state,
      error: payload.message,
      loading: false
    })
  ),
  on(
    UrlActions.deleteUrlStart,
    (state) => ({
      ...state,
      error: null,
      loading: true
    })
  ),
  on(
    UrlActions.deleteUrlSuccess,
    (state, payload) => ({
      ...state,
      loading: false,
      shortenedUrls: {
        items: state.shortenedUrls.items.filter(url => url._id !== payload.id),
        count: state.shortenedUrls.count - 1,
        total: state.shortenedUrls.total - 1
      },
    })
  ),
  on(
    UrlActions.deleteUrlFail,
    (state, payload) => ({
      ...state,
      error: payload.message,
      loading: false
    })
  ),
  on(
    UrlActions.getShortenedUrlsStart,
    (state, payload) => ({
      ...state,
      error: null,
      loading: true
    })
  ),
  on(
    UrlActions.getShortenedUrlsSuccess,
    (state, payload) => ({
      ...state,
      loading: false,
      shortenedUrls: payload.shortenedUrls
    })
  ),
  on(
    UrlActions.getShortenedUrlsFail,
    (state, payload) => ({
      ...state,
      error: payload.message,
      loading: false,
    })
  ),
);

export function urlReducer(state: State | undefined, action: Action) {
  return _urlReducer(state, action);
}
