import {Component, OnDestroy, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import {Store} from "@ngrx/store";
import * as fromApp from '../../../../store/app.reducer';
import * as AuthActions from '../../store/auth.actions';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {

  public isLoading = false;
  public errorMessage: string | null = null;

  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit(): void {
    this.subscription.add(this.watchAuthStateChanges());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  watchAuthStateChanges(): Subscription {
    return this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.loading;
      this.errorMessage = authState.error;
    });
  }

  onSubmit(form: NgForm): void {
		console.log('Submitted form: ', form);

		const { email, password } = form.value;

		this.store.dispatch(AuthActions.loginStart({email, password}));
	}
}
