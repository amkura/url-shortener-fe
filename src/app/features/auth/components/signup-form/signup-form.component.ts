import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../../store/app.reducer';
import * as AuthActions from '../../store/auth.actions';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit, OnDestroy {

  form!: FormGroup;

  public isLoading = false;
  public errorMessage: string | null = null;

  private subscription: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit(): void {
    this.subscription.add(this.watchAuthStateChanges());
    this.initializeForm();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  watchAuthStateChanges(): Subscription {
    return this.store.select('auth').subscribe(authState => {
      console.log('New authState: ', authState);
      this.isLoading = authState.loading;
      this.errorMessage = authState.error;
    });
  }

  initializeForm(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirm: ['', [Validators.required]],
    }, {
      validator: this.confirmPassword.bind(this)
    });
  }

  confirmPassword(formGroup: FormGroup): void {
    const {password, passwordConfirm} = formGroup.value;
    if (password !== passwordConfirm) {
      formGroup.get('passwordConfirm')?.setErrors({passwordMismatch: true});
    }
  }


  onSubmit(): void {
    console.log('Submitted form: ', this.form);
    const { email, password } = this.form.value;

    this.store.dispatch(AuthActions.signupStart({
      email, password
    }));
	}
}
