import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as AuthActions from './auth.actions';
import { HttpClient } from "@angular/common/http";
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import { of } from "rxjs";
import {Injectable} from "@angular/core";
import {User} from "../user.model";
import {Router} from "@angular/router";
import { environment} from "../../../../environments/environment";
import {ErrorResponse} from "../../shared/models/error-response.model";

export interface AuthResponseData {
  accessToken: string;
  email: string;
  id: string;
}

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private router: Router
  ) {}

  routeURL = environment.API;

  loginStart$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.loginStart),
        switchMap((action) => {
          const { email, password } = action;
          return this.http
            .post<AuthResponseData>(`${this.routeURL}/v1/user/login`, { email, password })
            .pipe(
              map(resData => AuthActions.loginSuccess(resData)),
              catchError((error: ErrorResponse) => of(AuthActions.loginFail({message: error.error.message})))
            )
        })
      )
  )

  loginSuccess$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.loginSuccess),
        tap((responseData) => {
          const { accessToken, email, id } = responseData;
          const user = new User(email, id, accessToken);
          localStorage.setItem('userData', JSON.stringify(user));
          this.router.navigate(['']);
        })
      )
  , {
    dispatch: false
    });

  signupStart$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.signupStart),
        switchMap((action) => {
          const { email, password } = action;
          return this.http
            .post<AuthResponseData>(`${this.routeURL}/v1/user/signup`, { email, password })
            .pipe(
              map(resData => AuthActions.signupSuccess(resData)),
              catchError((error: ErrorResponse) => of(AuthActions.loginFail({message: error.error.message})))
            )
        })
      )
  )

  signupSuccess$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.signupSuccess),
        tap(responseData => {
          const { accessToken } = responseData;
          localStorage.setItem('userData', JSON.stringify(accessToken));
          this.router.navigate(['']);
        })
      )
  , {
    dispatch: false
  });

  autologin$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.autoLogin),
        map(() => {
          const userData: AuthResponseData = JSON.parse(localStorage.getItem('userData')!);
          if(!userData) {
            return {type: 'dummy'};
          }

          const {email, id, accessToken} = userData;
          const loadedUser = new User(email, id, accessToken);

          if(!loadedUser.accessToken) {
            return { type: 'dummy' };
          }

          return AuthActions.loginSuccess({ email, id, accessToken });
        })
      )
  );

  logout$ = createEffect(() =>
    this.actions$
      .pipe(
        ofType(AuthActions.logout),
        tap(() => {
          localStorage.removeItem('userData');
          this.router.navigate(['/auth']);
        })
      )
  , {
    dispatch: false
    });
}
