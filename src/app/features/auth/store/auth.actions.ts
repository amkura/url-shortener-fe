import {  createAction, props } from "@ngrx/store";
import {AuthResponseData} from "./auth.effects";

export const loginStart = createAction(
  '[Auth] Login Start',
  props<{
    email: string,
    password: string
  }>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<AuthResponseData>()
);

export const loginFail = createAction(
  '[Auth] Login Fail',
  props<{
    message: string;
  }>()
);

export const signupStart = createAction(
  '[Auth] Signup Start',
  props<{
    email: string,
    password: string
  }>()
);

export const signupSuccess = createAction(
  '[Auth] Signup Success',
  props<AuthResponseData>()
);

export const signupFail = createAction(
  '[Auth] Signup Fail',
  props<{
    message: string;
  }>()
);

export const autoLogin = createAction(
  '[Auth] Auto Login'
);

export const logout = createAction(
  '[Auth] Logout'
);

