import { createReducer, on, Action } from "@ngrx/store"
import * as AuthActions from "./auth.actions"
import {User} from "../user.model";

export interface State {
  user: User | null
  error: string | null;
  loading: boolean;
}

const initialState: State = {
  user: null,
  error: null,
  loading: false
}

const _authReducer = createReducer(
  initialState,
  on(
    AuthActions.loginStart,
    AuthActions.signupStart,
    (state) => ({
      ...state,
      error: null,
      loading: true
    })
  ),
  on(
    AuthActions.loginSuccess,
    AuthActions.signupSuccess,
    (state, payload) => ({
      ...state,
      user: new User(payload.email, payload.id, payload.accessToken),
      error: null,
      loading: false
    })
  ),
  on(
    AuthActions.loginFail,
    AuthActions.signupFail,
    (state, payload) => ({
      ...state,
      error: payload.message,
      loading: false
    })
  ),
  on(
    AuthActions.autoLogin,
    (state) => ({
      ...state
    })
  ),
  on(
    AuthActions.logout,
    (state) => ({
      ...state,
      user: null
    })
  ),
);

export function authReducer(state: State | undefined, action: Action) {
  return _authReducer(state, action);
}
