import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthContainerComponent } from './components/auth/auth.container';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const COMPONENTS = [
	AuthContainerComponent,
  LoginFormComponent,
  SignupFormComponent,
];

const routes: Routes = [
  { path: '', component: AuthContainerComponent, children: [
    { path: '', component: LoginFormComponent },
    { path: 'signup', component: SignupFormComponent }
  ]}
]

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
		RouterModule.forChild(routes),
  ],
  providers: [],
})
export class AuthModule { }
