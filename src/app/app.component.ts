import {Component, Inject, OnDestroy, OnInit, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import {Store} from "@ngrx/store";
import * as fromApp from './store/app.reducer';
import * as AuthActions from './features/auth/store/auth.actions';
import {map} from "rxjs/operators";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  isAuthenticated = false;
  subscription = new Subscription();

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<fromApp.AppState>
  ) {
  }

  ngOnInit() {
    if(isPlatformBrowser(this.platformId)) {
      this.store.dispatch(AuthActions.autoLogin());
    }
    this.watchAuthenticatedState();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  watchAuthenticatedState() {
    this.subscription = this.store.select('auth')
      .pipe(
        map(authState => authState.user)
      )
      .subscribe(user => {
        this.isAuthenticated = !!user;
      });
  }

}
