import * as fromAuth from '../features/auth/store/auth.reducer';
import * as fromUrl from '../features/home/store/reducers/url.reducer';

import {ActionReducerMap} from "@ngrx/store";

export interface AppState {
  auth: fromAuth.State,
  url: fromUrl.State
}

export const appReducer: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  url: fromUrl.urlReducer
};
