import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {StoreModule} from "@ngrx/store";
import {AuthEffects} from "./features/auth/store/auth.effects";
import {EffectsModule} from "@ngrx/effects";
import {appReducer} from "./store/app.reducer";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptorService} from "./features/auth/auth-interceptor.service";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {environment} from "../environments/environment.prod";
import {UrlEffects} from "./features/home/store/effects/url.effects";
import {NavComponent} from "./features/shared/components/nav/nav.component";
import {UrlListComponent} from "./features/home/components/url-list/url-list.component";
import {UrlItemComponent} from "./features/home/components/url-list/url-item/url-item.component";
import {NgxPaginationModule} from "ngx-pagination";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    UrlListComponent,
    UrlItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    NgxPaginationModule,
    StoreModule.forRoot(appReducer),
    StoreDevtoolsModule.instrument({logOnly: !environment.production}),
    EffectsModule.forRoot([AuthEffects, UrlEffects]),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
